package io.renren.modules.zhihu.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.zhihu.entity.MyAbcEntity;
import io.renren.modules.zhihu.service.MyAbcService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 菜单管理
 *
 * @author Auto
 * @email 123@gmail.com
 * @date 2021-08-02 22:37:34
 */
@RestController
@RequestMapping("zhihu/myabc")
public class MyAbcController {
    @Autowired
    private MyAbcService myAbcService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("zhihu:myabc:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = myAbcService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{menuId}")
    @RequiresPermissions("zhihu:myabc:info")
    public R info(@PathVariable("menuId") Long menuId){
		MyAbcEntity myAbc = myAbcService.getById(menuId);

        return R.ok().put("myAbc", myAbc);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("zhihu:myabc:save")
    public R save(@RequestBody MyAbcEntity myAbc){
		myAbcService.save(myAbc);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("zhihu:myabc:update")
    public R update(@RequestBody MyAbcEntity myAbc){
		myAbcService.updateById(myAbc);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("zhihu:myabc:delete")
    public R delete(@RequestBody Long[] menuIds){
		myAbcService.removeByIds(Arrays.asList(menuIds));

        return R.ok();
    }

}
