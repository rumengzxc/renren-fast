package io.renren.modules.zhihu.dao;

import io.renren.modules.zhihu.entity.MyAbcEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 菜单管理
 * 
 * @author Auto
 * @email 123@gmail.com
 * @date 2021-08-02 22:37:34
 */
@Mapper
public interface MyAbcDao extends BaseMapper<MyAbcEntity> {
	
}
