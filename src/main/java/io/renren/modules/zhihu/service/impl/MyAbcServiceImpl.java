package io.renren.modules.zhihu.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.zhihu.dao.MyAbcDao;
import io.renren.modules.zhihu.entity.MyAbcEntity;
import io.renren.modules.zhihu.service.MyAbcService;


@Service("myAbcService")
public class MyAbcServiceImpl extends ServiceImpl<MyAbcDao, MyAbcEntity> implements MyAbcService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MyAbcEntity> page = this.page(
                new Query<MyAbcEntity>().getPage(params),
                new QueryWrapper<MyAbcEntity>()
        );

        return new PageUtils(page);
    }

}