package io.renren.modules.zhihu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.zhihu.entity.MyAbcEntity;

import java.util.Map;

/**
 * 菜单管理
 *
 * @author Auto
 * @email 123@gmail.com
 * @date 2021-08-02 22:37:34
 */
public interface MyAbcService extends IService<MyAbcEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

